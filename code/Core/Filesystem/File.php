<?php


namespace FOSSCMS\Core\Filesystem;


use FOSSCMS\Core\Exceptions\AliasNotFoundException;
use FOSSCMS\Core\Exceptions\FileNotOpenException;
use FOSSCMS\Core\Services\FilesystemService;

/**
 * Class File
 * @package FOSSCMS\Core\Filesystem
 */
class File
{
    /**
     * @var FilesystemService
     */
    protected $fs;

    /**
     * @var string
     */
    protected $resourcePath;
    /**
     * @var null|resource
     */
    protected $handle = null;

    /**
     * File constructor.
     * @param FilesystemService $fs
     * @param $resourcePath
     */
    public function __construct(FilesystemService $fs, $resourcePath)
    {
        $this->fs = $fs;
        $this->resourcePath = $resourcePath;
    }

    /**
     * File destructor.
     */
    public function __destruct()
    {
        $this->close();
    }

    /**
     * @return $this
     * @throws FileNotOpenException
     */
    protected function _ensureOpen()
    {
        if (!$this->isOpen()) {
            throw new FileNotOpenException($this->resourcePath);
        }

        return $this;
    }

    /**
     * @return null|resource
     */
    public function getHandle()
    {
        return $this->handle;
    }

    /**
     * Returns the resource path of the current file
     * @return string
     */
    public function getResourcePath(): string
    {
        return $this->resourcePath;
    }

    /**
     * Returns the real path of the current file
     * @return string
     * @throws AliasNotFoundException
     */
    public function getRealPath(): string
    {
        return $this->fs->resolveResourcePath($this->getResourcePath());
    }

    /**
     * @return bool
     */
    public function isOpen(): bool
    {
        return $this->handle !== null;
    }

    /**
     * @param string $mode
     * @throws AliasNotFoundException
     */
    public function open(string $mode = "r"): void
    {
        $this->handle = fopen($this->fs->resolveResourcePath($this->resourcePath), $mode);
    }

    /**
     * Closes the file handle if it's open
     */
    public function close(): void
    {
        if ($this->isOpen()) {
            fclose($this->handle);
            $this->handle = null;
        }
    }

    /**
     * @param int $length
     * @return string
     * @throws FileNotOpenException
     */
    public function read(int $length): string
    {
        return fread($this->_ensureOpen()->getHandle(), $length);
    }

    /**
     * @todo Find a better way to do variadic arguments
     * @param string $string
     * @param int|null $length
     * @return int
     * @throws FileNotOpenException
     */
    public function write(string $string, ?int $length = null): int
    {
        if ($length === null) {
            return fwrite($this->_ensureOpen()->getHandle(), $string);
        } else {
            return fwrite($this->_ensureOpen()->getHandle(), $string, $length);
        }
    }

    /**
     * @param int $offset
     * @param int $where
     * @return int
     * @throws FileNotOpenException
     */
    public function seek(int $offset, int $where = SEEK_SET): int
    {
        return fseek($this->_ensureOpen()->getHandle(), $offset, $where);
    }
}