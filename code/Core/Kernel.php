<?php

namespace FOSSCMS\Core;

use FOSSCMS\Core\Filesystem\LocalFileSystem;
use FOSSCMS\Core\Services\CacheService;
use FOSSCMS\Core\Services\DatabaseService;
use FOSSCMS\Core\Services\FilesystemService;
use FOSSCMS\Core\Services\PageService;
use FOSSCMS\Core\Traits\EventEmitterTrait;
use FOSSCMS\Core\Traits\ExtendableTrait;
use FOSSCMS\Core\Traits\SingletonTrait;
use FOSSCMS\Core\Traits\ServiceProviderTrait;
use FOSSCMS\Core\Services\ConfigService;
use FOSSCMS\Core\Services\LoggingService;
use FOSSCMS\Core\Services\RoutingService;
use FOSSCMS\Core\Services\CryptoService;
use FOSSCMS\Core\Services\SessionService;
use FOSSCMS\Core\Services\YamlService;

class Kernel
{
    use SingletonTrait;
    use ServiceProviderTrait;
    use ExtendableTrait;
    use EventEmitterTrait;

    protected function __construct()
    {
        $this->registerCoreServices();
    }

    protected function registerCoreServices(): void
    {
        $this->registerService("fs", FilesystemService::class);
        $this->registerService("config", ConfigService::class);
        $this->registerService("log", LoggingService::class);
        $this->registerService("route", RoutingService::class);
        $this->registerService("page", PageService::class);
        $this->registerService("crypto", CryptoService::class);
        $this->registerService("session", SessionService::class);
        $this->registerService("cache", CacheService::class);
        $this->registerService("database", DatabaseService::class);
        $this->registerService("yaml", YamlService::class);
    }


    protected function initializeCoreServices(): void
    {
        /** @var FilesystemService $fs */
        $fs = $this->getService("fs");

        // Set fs root
        $fs->setRoot(__DIR__ . "/../..");

        // Create aliases
        $fs->addAliasByResourcePath("config", "root://config");

        $fs->addAliasByResourcePath("public", "root://public");
        $fs->addAliasByResourcePath("cache", "public://cache");

        $fs->addAliasByResourcePath("content", "root://content");
        $fs->addAliasByResourcePath("database", "content://data");

        // Set default template directory
        /** @var PageService $page */
        $page = $this->getService("page");
        $page->addTemplateDirectory("content://pages");

        // Initialize page service event listeners
        $page->initializeEvents();
    }

    public function initializeCore(): void
    {
        $this->initializeCoreServices();
    }

    public function initializeAll(): void
    {
        $this->initializeCore();
        $this->initializeExtensions();
    }

    public function handleRequest(): bool
    {
        /** @var RoutingService $route */
        $route = $this->getService("route");
        return $route->handleRequest();
    }
}
