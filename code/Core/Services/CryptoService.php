<?php

/*

    This service creates hashes from data

*/

namespace FOSSCMS\Core\Services;

use FOSSCMS\Core\Services\ServiceInterface;

class CryptoService implements ServiceInterface
{
    // Hash a password using bcrypt
    public function bcrypt($data, $cost = 12)
    {
        return password_hash($data, PASSWORD_BCRYPT, ['cost' => $cost]);
    }

    // Hash data or passwords using sha512
    public function sha512($data, bool $apply_salt = false, $salt = null)
    {
        // If a user wants to apply salt, they probably want to hash their password
        if($apply_salt)
        {
            if($salt === null)
            {
                // Generate salt
                $salt = $this->gererateSalt();
            }
            // Generate initial hash
            $hash = hash('sha512', $data . $salt);
            // Create a hash with salt
            for ($key=0; $key < 20000; $key++) {
                $hash = hash('sha512', $hash . $salt);
            }
            return array($hash, $salt);
        }
        else
        {
            return hash('sha512', $data);
        }
    }

    // Generate a salt, mostly used in sha512 hash creation
    public function gererateSalt($length = 69)
    {
        return random_bytes($length);
    }
}
