<?php

/*

    This file renders a page using twig

*/

namespace FOSSCMS\Core\Services;

use FOSSCMS\Core\Kernel;
use FOSSCMS\Core\Services\OldFS;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class PageService implements ServiceInterface
{
    protected $template_directories;
    protected $global_variables;
    protected $css_files;
    protected $js_files;

    protected $first_template;

    protected $fs_loader;
    protected $twig;

    public function __construct()
    {
        $this->template_directories = [];
        $this->global_variables = [];
        $this->css_files = [];
        $this->js_files = [];

        $this->first_template = true;
    }

    public function initializeTwig()
    {
        $this->fs_loader = new FilesystemLoader($this->template_directories);
        $this->twig = new Environment($this->fs_loader);
    }

    public function initializeEvents()
    {
        /** @var Kernel $kernel */
        $kernel = Kernel::instance();
        $kernel->on("rendering.start", function () use ($kernel) {
            $this->addGlobalVariable("css", $this->css_files);
            $this->addGlobalVariable("javascript", $this->js_files);
        });
    }

    public function renderTemplate(string $filename, array $variables = []): string
    {
        if ($this->twig === null) {
            $this->initializeTwig();
        }

        if ($this->first_template) {
            $kernel = Kernel::instance();
            $kernel->fire("rendering.start");
            $this->first_template = false;
        }

        $template = $this->twig->load($filename);
        return $template->render(array_merge($this->global_variables, $variables));
    }

    public function addTemplateDirectory(string $path): void
    {
        /** @var Kernel $kernel */
        $kernel = Kernel::instance();

        /** @var FilesystemService $fs */
        $fs = $kernel->getService("fs");
        $this->template_directories[] = $fs->resolveResourcePath($path);
    }

    public function addGlobalVariable($name, $value)
    {
        $this->global_variables[$name] = $value;
    }

    public function addCSSFile(string $path): void
    {
        /** @var Kernel $kernel */
        $kernel = Kernel::instance();

        /** @var OldFS $fs */
        $fs = $kernel->getService("fs");

        $path = $fs->getPublicPath($path);
        $this->css_files[] = $path;
    }

    public function addJavascriptFile(string $path): void
    {
        /** @var Kernel $kernel */
        $kernel = Kernel::instance();

        /** @var OldFS $fs */
        $fs = $kernel->getService("fs");

        $path = $fs->getPublicPath($path);
        $this->js_files[] = $path;
    }
}
