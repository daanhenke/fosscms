<?php

/*

    This service acts like a database of some sort

*/

namespace FOSSCMS\Core\Services;

use FOSSCMS\Core\Helpers\DebugUtils;
use FOSSCMS\Core\Services\ServiceInterface;
use FOSSCMS\Core\Kernel;

class DatabaseService implements ServiceInterface
{
    // Create a new table
    public function createTable(string $table): bool
    {
        /** @var FilesystemService $fs */
        $fs = Kernel::instance()->getService("fs");

        // Check if the table already exists
        if(!$this->_tableExists($table))
        {
            // Create the new table
            $fs->mkdir("database://$table");
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
    * @todo DatabaseService uses PHP's builtin rmdir() for now, since FilesystemService doesnt have an inplementation of rmdir()/unlink() yet
    */
    public function removeTable(string $table): bool
    {
        /** @var FilesystemService $fs */
        $fs = Kernel::instance()->getService("fs");

        // Check if the table exists
        if($this->_tableExists($table))
        {
            rmdir($fs->resolveResourcePath("database://$table"));
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @todo: Insert function only creates a file at the moment
     */
    public function insert(string $table/*, \stdClass $contents*/): bool
    {
        /** @var FilesystemService $fs */
        $fs = Kernel::instance()->getService("fs");

        // Find the latest entry
        $latestEntryID = $this->_findLatestColumnID($table);

        // Create a new entry file
        $fs->touch("database://" . $table . "/" . ((string) $latestEntryID+1) . ".yml");
        die("Item aangemaakt. ID van nieuwste entry: " . $this->_findLatestColumnID($table));
        return true;
    }

    private function _tableExists($table): bool
    {
        /** @var FilesystemService $fs */
        $fs = Kernel::instance()->getService("fs");

        return $fs->isDirectory("database://$table");
    }

    /**
    * @todo FilesystemService has no way of checking if a folder is empty or not. So until then, DatabaseService uses a custom solution.
    */
    private function _findLatestColumnID($table): int
    {
        /** @var FilesystemService $fs */
        $fs = Kernel::instance()->getService("fs");

        $files = scandir($fs->resolveResourcePath("database://$table"));
        if(count($files) <= 2)
        {
            // No columns exist yet, return -1
            return -1;
        }
        // Natural sort the files array
        natsort($files);
        // Return the name of the latest file
        return explode('.yml', end($files))[0];
    }
}
