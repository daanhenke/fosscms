<?php

namespace FOSSCMS\Core\Services;

use FOSSCMS\Core\Exceptions\EntryNotFoundException;
use FOSSCMS\Core\Exceptions\FileNotOpenException;
use FOSSCMS\Core\Filesystem\FilesystemInterface;
use FOSSCMS\Core\Helpers\ArrayUtils;
use FOSSCMS\Core\Kernel;

class ConfigService implements ServiceInterface
{

    /**
     * @param string $resourcePath
     * @param \stdClass $contents
     */
    public function writeConfigFile(string $resourcePath, \stdClass $contents): \stdClass
    {
        $yamlService = Kernel::instance()->getService('yaml');
        return $yamlService->writeYamlFile($resourcePath, $contents);
    }

    /**
     * @param string $filename
     * @param string $key
     * @param null $default
     * @param bool $writeDefault
     * @return mixed|null
     * @throws \Exception
     */
    public function get(string $filename, string $key, $default = null, bool $writeDefault = false)
    {
        $yamlService = Kernel::instance()->getService('yaml');
        $configData = $yamlService->readYamlFile("config://$filename.yml");

        $value = ArrayUtils::getValueFromClass($configData, $key);

        if ($value === null) {
            if ($writeDefault) {
                $this->set($filename, $key, $default);
            }

            return $default;
        }

        return $value;
    }

    /**
     * @param string $filename
     * @param string $key
     * @param $value
     * @throws EntryNotFoundException
     * @throws FileNotOpenException
     */
    public function set(string $filename, string $key, $value)
    {
        $yamlService = Kernel::instance()->getService('yaml');
        $configData = $yamlService->readYamlFile("config://$filename.yml");

        ArrayUtils::setValueOnClass($configData, $key, $value);

        $yamlService->writeYamlFile("config://$filename.yml", $configData);
    }
}
