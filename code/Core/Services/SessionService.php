<?php

/*

    This service handles session variables

*/

namespace FOSSCMS\Core\Services;

use FOSSCMS\Core\Services\ServiceInterface;

class SessionService implements ServiceInterface
{
    public function __construct()
    {
        // Check if a session had already been started
        if (session_status() == PHP_SESSION_NONE) {
            // Start a new session
            session_start();
        }
    }

    public function get(string $itemName)
    {
        return $_SESSION[$itemName];
    }

    public function set(string $itemName, $value)
    {
        $_SESSION[$itemName] = $value;
    }

    public function getAll()
    {
        return $_SESSION;
    }
}
