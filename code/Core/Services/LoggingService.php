<?php

namespace FOSSCMS\Core\Services;

use FOSSCMS\Core\Services\ServiceInterface;

class LoggingService implements ServiceInterface
{
    protected $messages;
    protected $categories;

    public function __construct()
    {
        $this->messages = [];
        $this->categories = [];

        $this->addCategory("error", 0, function ($message) {
            error_log($message);
        });

        $this->addCategory("warn", 100);

        $this->addCategory("info", 200);

        $this->addCategory("verbose", 300);

        $this->addCategory("debug", 400);
    }

    public function __call($function, $arguments)
    {
        if (isset($this->categories[$function])) {
            $this->raw($function, $arguments[0]);
        }
    }

    public function raw(string $category, string $message): void
    {
        if (! isset($this->categories[$category])) {
            $this->error("Tried to log to invalid category: $category");
            return;
        }

        $data = $this->categories[$category];
        if (isset($data["callback"])) {
            $data["callback"]($message);
        }

        echo "[" . $category . "]: " . $message;
        $this->messages[$category][] = $message;
    }

    public function addCategory(string $name, int $severity = 0, ?callable $callback = null): void
    {
        if (isset($this->categories[$name])) {
            throw new \Exception("Category '$name' already defined");
        }

        $this->categories[$name] = [
            "callback" => $callback,
            "severity" => $severity
        ];

        $this->messages[$name] = [];
    }
}