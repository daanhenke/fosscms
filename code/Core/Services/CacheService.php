<?php


namespace FOSSCMS\Core\Services;


use FOSSCMS\Core\Exceptions\FileNotFoundException;
use FOSSCMS\Core\Helpers\DebugUtils;
use FOSSCMS\Core\Kernel;

class CacheService implements ServiceInterface
{
    public function forceCacheFile(string $resourcePath): string
    {
        /** @var Kernel $kernel */
        $kernel = Kernel::instance();

        /** @var FilesystemService $fs */
        $fs = $kernel->getService("fs");

        if (! $fs->isFile($resourcePath)) {
            throw new FileNotFoundException($resourcePath);
        }

        /** @var CryptoService $crypto */
        $crypto = $kernel->getService("crypto");

        $hash = $crypto->sha512($fs->getRelativePathFromRoot($resourcePath));
        $extension = $fs->getExtension($resourcePath);

        $outFile = "cache://$extension/$hash.$extension";
        $outDir = $fs->getDirectoryName($outFile);

        if (! $fs->exists($outDir)) {
            $fs->mkdir($outDir, 0777, true);
        }

        $fs->copy($resourcePath, $outFile);
        return $outFile;
    }
}