<?php


namespace FOSSCMS\Core\Services;


use FOSSCMS\Core\Exceptions\AliasNotFoundException;
use FOSSCMS\Core\Exceptions\EntryNotFoundException;
use FOSSCMS\Core\Exceptions\PathOutsideOfAliasException;
use FOSSCMS\Core\Filesystem\File;

class FilesystemService implements ServiceInterface
{
    /**
     * @var string
     */
    protected $root;

    /**
     * @var array
     */
    protected $aliases;

    /**
     * FilesystemService constructor.
     */
    public function __construct()
    {
        $this->root = "";
        $this->aliases = [];
    }

    /**
     * @param string $rootPath
     */
    public function setRoot(string $rootPath): void
    {
        $this->root = realpath($rootPath);

        $this->addAliasByRealPath("root", $this->root);
    }

    /**
     * @param string $path
     * @return string
     */
    public function _toAbsolutePath(string $path): string
    {
        $inChunks = explode("/", $path);
        $outChunks = [];

        foreach ($inChunks as $inChunk)
        {
            if ($inChunk === "..") {
                if (count($outChunks) > 0) {
                    array_pop($outChunks);
                }
            }
            elseif ($inChunk === ".")
            {
                continue;
            }
            else {
                array_push($outChunks, $inChunk);
            }
        }

        $out = implode("/", $outChunks);
        if (substr($out, 0, 1) !== "/") {
            $out = "/$out";
        }

        return $out;
    }

    /**
     * @param string $resourcePath
     * @return string|null
     * @throws EntryNotFoundException
     */
    public function _ensureExists(string $resourcePath): ?string
    {
        if ($this->exists($resourcePath)) {
            return $resourcePath;
        }

        throw new EntryNotFoundException($resourcePath);
    }

    /**
     * @param string $path
     * @return string
     */
    public function _ensureUnixPath(string $path): string
    {

        $path = str_replace("\\", "/", $path);
        $path = preg_replace("|(?<=.)/+|", "/", $path);

        if (substr($path, 1, 1) === ":") {
            $path = substr($path, 2);
        }

        return $path;
    }

    /**
     * @param string $resourcePath
     * @return string
     * @throws AliasNotFoundException
     */
    public function resolveResourcePath(string $resourcePath): string
    {
        $resourcePathChunks = explode(":/", $resourcePath);

        if (count($resourcePathChunks) !== 2) {
            throw new \InvalidArgumentException();
        }

        return $this->_toAbsolutePath($this->getAliasRealPath($resourcePathChunks[0]) . $resourcePathChunks[1]);
    }

    /**
     * @param string $realPath
     * @param string $alias
     * @return bool
     * @throws AliasNotFoundException
     */
    public function isRealpathInAlias(string $realPath, string $alias): bool
    {
        return (strpos($realPath, $this->getAliasRealPath($alias))) !== false;
    }

    /**
     * @param string $resourcePath
     * @return string
     * @throws AliasNotFoundException
     */
    public function getRelativePathFromRoot(string $resourcePath): string
    {
        $realPath = $this->resolveResourcePath($resourcePath);

        if (strpos($realPath, $this->root) === false) {
            throw new \ParseError();
        }

        return substr($realPath, strlen($this->root));
    }

    /**
     * @param string $alias
     * @return string
     * @throws AliasNotFoundException
     */
    public function getAliasRealPath(string $alias): string
    {
        if ($this->hasAlias($alias)) {
            return $this->aliases[$alias];
        }

        throw new AliasNotFoundException($alias);
    }

    /**
     * @param string $path
     * @param string $alias
     * @return string
     * @throws AliasNotFoundException
     * @throws PathOutsideOfAliasException
     */
    public function toResourcePath(string $path, string $alias = "root"): string
    {
        if ($this->isRealpathInAlias($path, $alias)) {
            return "$alias:/" .substr($path, strlen($this->getAliasRealPath($alias)));
        }

        throw new PathOutsideOfAliasException($path, $alias);
    }

    /**
     * @param string $alias
     * @return bool
     */
    public function hasAlias(string $alias): bool
    {
        return isset($this->aliases[$alias]);
    }

    /**
     * @param string $alias
     * @param string $path
     */
    public function addAliasByRealPath(string $alias, string $path): void
    {
        $this->aliases[$alias] = $path;
    }

    /**
     * @param string $alias
     * @param string $resourcePath
     * @throws AliasNotFoundException
     */
    public function addAliasByResourcePath(string $alias, string $resourcePath): void
    {
        $this->aliases[$alias] = $this->resolveResourcePath($resourcePath);
    }

    /**
     * @param string $resourcePath
     * @return bool
     * @throws AliasNotFoundException
     */
    public function isFile(string $resourcePath): bool
    {
        return is_file($this->resolveResourcePath($resourcePath));
    }

    /**
     * @param string $resourcePath
     * @return bool
     * @throws AliasNotFoundException
     */
    public function isDirectory(string $resourcePath): bool
    {
        return is_dir($this->resolveResourcePath($resourcePath));
    }

    /**
     * @param string $resourcePath
     * @return bool
     * @throws AliasNotFoundException
     */
    public function exists(string $resourcePath)
    {
        return file_exists($this->resolveResourcePath($resourcePath));
    }

    /**
     * @param string $resourcePath
     * @param int|null $time
     * @throws AliasNotFoundException
     */
    public function touch(string $resourcePath, int $time = null): void
    {
        if ($time === null) {
            $time = time();
        }
        touch($this->resolveResourcePath($resourcePath), $time);
    }

    /**
     * @param string $resourcePath
     * @param int $mode
     * @param bool $recursive
     * @throws AliasNotFoundException
     */
    public function mkdir(string $resourcePath, int $mode = 0777, bool $recursive = false): void
    {
        mkdir($this->resolveResourcePath($resourcePath), $mode, $recursive);
    }

    /**
     * @param string $sourceResourcePath
     * @param string $destResourcePath
     * @throws EntryNotFoundException
     * @throws AliasNotFoundException
     */
    public function copy(string $sourceResourcePath, string $destResourcePath): void
    {
        copy(
            $this->resolveResourcePath($this->_ensureExists($sourceResourcePath)),
            $this->resolveResourcePath($destResourcePath)
        );
    }

    /**
     * @param string $resourcePath
     * @return string
     */
    public function getAliasName(string $resourcePath): string
    {
        $resourcePathChunks = explode(":/", $resourcePath);

        if (count($resourcePathChunks) !== 2) {
            throw new \InvalidArgumentException();
        }

        return $resourcePathChunks[0];
    }

    /**
     * @param string $resourcePath
     * @return string
     */
    public function getRelativeResource(string $resourcePath): string
    {
        $resourcePathChunks = explode(":/", $resourcePath);

        if (count($resourcePathChunks) !== 2) {
            throw new \InvalidArgumentException();
        }

        return $resourcePathChunks[1];
    }

    /**
     * @param string $resourcePath
     * @return string
     * @throws AliasNotFoundException
     */
    public function getExtension(string $resourcePath): string
    {
        return pathinfo($this->resolveResourcePath($resourcePath), PATHINFO_EXTENSION);
    }

    /**
     * @param string $resourcePath
     * @param int $levels
     * @return string
     * @throws AliasNotFoundException
     * @throws PathOutsideOfAliasException
     */
    public function getDirectoryName(string $resourcePath, int $levels = 1): string
    {
        $realPath = $this->resolveResourcePath($resourcePath);
        $dirname = dirname($realPath, $levels);

        $alias = $this->getAliasName($resourcePath);
        if (!$this->isRealpathInAlias($dirname, $alias)) {
            $alias = "root";
        }

        $unixDirname = $this->getRelativeResource($this->toResourcePath($dirname, $alias));
        return "$alias:/$unixDirname";
    }

    /**
     * @param string $resourcePath
     * @return File
     */
    public function getFile(string $resourcePath): File
    {
        return new File($this, $resourcePath);
    }
}