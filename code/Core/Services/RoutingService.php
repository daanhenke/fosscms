<?php

/*

    This file handles all the requests and executes a function within the given controller.
    FOSSCMS has one builtin controller, called "PageController".

*/

namespace FOSSCMS\Core\Services;

use FOSSCMS\Core\Services\ServiceInterface;

use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\HttpFoundation\Request;

class RoutingService implements ServiceInterface
{
    protected $routes;

    public function __construct()
    {
        $this->routes = new RouteCollection();
    }

    public function addRoute(string $path, string $controller, string $function): void
    {
        $this->routes->add($function, new Route($path, array(
                'controller' => $controller,
                'function' => $function
        )));
    }

    public function handleRequest(): bool
    {
        $context = new RequestContext();
        $request = Request::createFromGlobals();
        $context->fromRequest($request);
        $matcher = new UrlMatcher($this->routes, $context);

        // Find the current route
        try {
            $parameters = $matcher->match($context->getPathInfo());
        }
        catch(\Exception $exception) {
            return false;
        }
        $currentController = new $parameters["controller"];

        // Execute the controller
        call_user_func([$currentController, $parameters["_route"]], $request);
        return true;
    }

    public function getRoutes(): array
    {
        $routes = [];
        foreach ($this->routes as $key => $route) {
            $routes[] = $this->convertRouteToArray($route);
        }
        return $routes;
    }

    private function convertRouteToArray(Route $route)
    {
        return array($route->getPath(), $route->getDefault('controller') . '@' . $route->getDefault('function'));
    }
}
