<?php

namespace FOSSCMS\Core\Services;

use FOSSCMS\Core\Exceptions\EntryNotFoundException;
use FOSSCMS\Core\Exceptions\FileNotOpenException;
use FOSSCMS\Core\Filesystem\FilesystemInterface;
use FOSSCMS\Core\Helpers\ArrayUtils;
use FOSSCMS\Core\Kernel;
use Symfony\Component\Yaml\Yaml;

class YamlService implements ServiceInterface
{
    /**
     * @param string $resourcePath
     * @return \stdClass
     * @throws \Exception
     */
    public function readYamlFile(string $resourcePath): \stdClass
    {
        /** @var FilesystemService $fs */
        $fs = Kernel::instance()->getService("fs");

        if (!$fs->isFile($resourcePath)) {
            return new \stdClass();
        }

        $file = $fs->getFile($resourcePath);

        $array = Yaml::parseFile($file->getRealPath());
        if ($array === null) {
            return new \stdClass();
        }

        return ArrayUtils::arrayToStdClass($array);
    }

    /**
     * @todo Move to yaml service
     * @param string $resourcePath
     * @param \stdClass $contents
     * @throws EntryNotFoundException
     * @throws FileNotOpenException
     */
    public function writeYamlFile(string $resourcePath, \stdClass $contents)
    {
        /** @var FilesystemService $fs */
        $fs = Kernel::instance()->getService("fs");

        if (! $fs->exists($fs->getDirectoryName($resourcePath))) {
            $fs->mkdir($fs->getDirectoryName($resourcePath));
        }

        $fs->touch($resourcePath);
        $file = $fs->getFile($resourcePath);

        if ($file === null) {
            throw new \Exception("io error");
        }

        $file->open("w");
        $file->write(Yaml::dump(ArrayUtils::stdClassToArray($contents), 100));
    }

}
