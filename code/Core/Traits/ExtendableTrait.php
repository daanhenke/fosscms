<?php


namespace FOSSCMS\Core\Traits;


use FOSSCMS\Core\ExtensionInterface;

trait ExtendableTrait
{
    protected $__extensions = [];

    public function addExtension(ExtensionInterface $extension)
    {
        $this->__extensions[] = $extension;
    }

    public function initializeExtensions()
    {
        /** @var ExtensionInterface $extension */
        foreach ($this->__extensions as $extension)
        {
            $extension->initialize();
        }
    }
}