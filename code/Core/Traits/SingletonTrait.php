<?php

namespace FOSSCMS\Core\Traits;

trait SingletonTrait
{
    protected static $__instance = null;

    public static function instance()
    {
        if (static::$__instance === null) {
            static::$__instance = new static;
        }

        return static::$__instance;
    }

    protected function __construct() {}
}