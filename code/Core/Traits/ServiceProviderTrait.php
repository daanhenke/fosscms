<?php

namespace FOSSCMS\Core\Traits;

use FOSSCMS\Core\Exceptions\ServiceAlreadyRegisteredException;
use FOSSCMS\Core\Services\ServiceInterface;

trait ServiceProviderTrait
{
    protected $__services = [];

    /**
     * @param string $name
     * @param mixed $class
     * @throws ServiceAlreadyRegisteredException
     */
    public function registerService(string $name, $class): void
    {
        if (in_array($name, $this->__services)) {
            throw new ServiceAlreadyRegisteredException($name);
        }

        $this->__services[$name] = new $class;
    }

    /**
     * @param string $name
     * @return ServiceInterface|null
     */
    public function getService(string $name): ?ServiceInterface
    {
        if (isset($this->__services[$name])) {
            return $this->__services[$name];
        }

        return null;
    }
}