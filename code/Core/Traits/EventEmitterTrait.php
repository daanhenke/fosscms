<?php


namespace FOSSCMS\Core\Traits;


trait EventEmitterTrait
{
    protected $__listeners = [];

    public function on(string $event, callable $callback): void
    {
        if (! isset($this->__listeners[$event])) {
            $this->__listeners[$event] = [];
        }

        $this->__listeners[$event][] = $callback;
    }

    public function fire(string $event, ?array $data = null): void
    {
        if (! isset($this->__listeners[$event])) {
            return;
        }

        foreach ($this->__listeners[$event] as $callback) {
            $callback($data);
        }
    }
}