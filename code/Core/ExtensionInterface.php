<?php


namespace FOSSCMS\Core;


interface ExtensionInterface
{
    public function initialize();
}