<?php

// Page Controller

namespace FOSSCMS\Core\Controllers;

use FOSSCMS\Core\Helpers\DebugUtils;
use FOSSCMS\Core\Services\ServiceInterface;

class PageController implements ServiceInterface
{
    public function home($request) {
        // TODO: make twig work with this instead of giving a simple message
        echo("Welcome to the home page.");
        DebugUtils::dump($request);
        die('WAUW');
    }

    public function test($request) {
        // TODO: make twig work with this instead of giving a simple message
        die("This is a test page");
    }
}
