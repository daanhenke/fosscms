<?php


namespace FOSSCMS\Core\Helpers;


class ArrayUtils
{
    public static function arrayToStdClass(array $in): \stdClass
    {
        $out = new \stdClass();

        foreach ($in as $key => $value) {
            if (! strlen($key)) {
                continue;
            }

            if (is_array($value)) {
                $out->$key = self::arrayToStdClass($value);
            }
            else {
                $out->$key = $value;
            }
        }

        return $out;
    }

    public static function stdClassToArray(\stdClass $in): array
    {
        $out = [];

        foreach ((array)$in as $key => $value) {
            if (is_object($value)) {
                $out[$key] = self::stdClassToArray($value);
            }
            else {
                $out[$key] = $value;
            }
        }

        return $out;
    }

    public static function getValueFromClass(\stdClass $class, string $key)
    {
        // Split our key into selectors
        $chunks = explode(".", $key);

        // Get our first selector
        $key = $chunks[0];

        if (isset($class->$key)) {
            $value = $class->$key;

            // This is the last selector so return it's value
            if (count($chunks) === 1) {
                return $value;
            }
            // This is not our last selector so recursively continue until we are at the last selector
            else {
                // Get back our key without the first selector
                $key = implode(".", array_slice($chunks, 1));

                // Handle next selector
                return self::getValueFromClass($value, $key);
            }
        }

        // Selector not found, return null
        return null;
    }

    public static function setValueOnClass(\stdClass $class, string $key, $value): void
    {
        // Split our key into seperate selectors
        $chunks = explode(".", $key);

        // Loop trough our selectors
        while (sizeof($chunks) > 1) {
            // Get our current selector
            $currentKey = $chunks[0];

            // If $class does not contain our current selector, make a new stdClass so we can continue
            if (! isset($class->$currentKey)) {
                $class->$currentKey = new \stdClass();
            }

            // Set class to our new class
            $class = $class->$currentKey;

            // Remove the first selector
            $chunks = array_slice($chunks, 1);
        }

        // Get the last selector
        $currentKey = $chunks[0];

        // Set the actual value on the class
        $class->$currentKey = $value;
    }
}