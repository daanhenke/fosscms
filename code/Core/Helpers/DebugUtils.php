<?php


namespace FOSSCMS\Core\Helpers;


class DebugUtils
{
    public static function dump($object): void
    {
        $gotExecutedAt = debug_backtrace()[0];
        $file = $gotExecutedAt["file"];
        $line = $gotExecutedAt["line"];

        $backtracePretty = "$file:$line";

        if (php_sapi_name() === "cli")
        {
            echo "Dump: ";
        }
        else
        {
            echo "<h5>Dump: ($backtracePretty)</h5><pre>";
        }

        var_dump($object);

        if (php_sapi_name() === "cli")
        {
            echo "\n";
        }
        else
        {
            echo "</pre><br>";
        }
    }

    public static function dd($object): void
    {
        static::dump($object);
        die();
    }
}