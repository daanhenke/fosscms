<?php


namespace FOSSCMS\Core\Exceptions;


use Throwable;

class ServiceAlreadyRegisteredException extends \Exception
{
    public function __construct($service = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($service, $code, $previous);
    }
}