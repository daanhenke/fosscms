<?php


namespace FOSSCMS\Core\Exceptions;


use FOSSCMS\Core\Helpers\DebugUtils;
use FOSSCMS\Core\Kernel;
use FOSSCMS\Core\Services\FilesystemService;
use Throwable;

class EntryNotFoundException extends \Exception
{
    public function __construct(string $resourcePath = "", int $code = 0, Throwable $previous = null)
    {
        /** @var Kernel $kernel */
        $kernel = Kernel::instance();

        /** @var FilesystemService $fs */
        $fs = $kernel->getService("fs");

        $realPath = $fs->resolveResourcePath($resourcePath);
        parent::__construct("$resourcePath ($realPath)", $code, $previous);
    }
}
