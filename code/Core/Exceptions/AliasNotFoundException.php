<?php


namespace FOSSCMS\Core\Exceptions;


use Throwable;

class AliasNotFoundException extends \Exception
{
    public function __construct($alias = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($alias, $code, $previous);
    }
}