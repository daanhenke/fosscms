<?php


namespace FOSSCMS\Core\Exceptions;


use Throwable;

class PathOutsideOfAliasException extends \Exception
{
    public function __construct($path = "", $alias = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct("$path : $alias", $code, $previous);
    }
}