<?php


namespace FOSSCMS\Admin\Service;


use FOSSCMS\Core\Kernel;
use FOSSCMS\Core\Services\PageService;
use FOSSCMS\Core\Services\ServiceInterface;

class AdminService implements ServiceInterface
{
    protected $sidebarEntries;

    public function __construct()
    {
        $this->sidebarEntries = [];
    }

    public function addSidebarEntry(string $url, string $name)
    {
        $this->sidebarEntries[] = [
            "url" => $url,
            "name" => $name
        ];
    }

    public function getSidebarEntries(): array
    {
        return $this->sidebarEntries;
    }

    public function addAdminCSS(): void
    {
        /** @var Kernel $kernel */
        $kernel = Kernel::instance();

        /** @var PageService $page */
        $page = $kernel->getService("page");

        $page->addCSSFile("root://code/Admin/content/css/test.css");
    }

    public function addAdminJavascript(): void
    {
        /** @var Kernel $kernel */
        $kernel = Kernel::instance();

        /** @var PageService $page */
        $page = $kernel->getService("page");

        $page->addJavascriptFile("root://code/Admin/content/js/test.js");
    }
}