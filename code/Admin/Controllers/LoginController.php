<?php


namespace FOSSCMS\Admin\Controllers;


use FOSSCMS\Admin\Service\AdminService;
use FOSSCMS\Core\Kernel;
use FOSSCMS\Core\Services\PageService;
use Symfony\Component\HttpFoundation\Request;

class LoginController
{
    public function login(Request $request)
    {
        /** @var Kernel $kernel */
        $kernel = Kernel::instance();

        /** @var PageService $page */
        $page = $kernel->getService("page");

        /** @var AdminService $admin */
        $admin = $kernel->getService("admin");
        $admin->addAdminCSS();
        $admin->addAdminJavascript();

        echo $page->renderTemplate("test.twig");
    }
}