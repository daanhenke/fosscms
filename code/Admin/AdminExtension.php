<?php


namespace FOSSCMS\Admin;


use FOSSCMS\Admin\Service\AdminService;
use FOSSCMS\Core\ExtensionInterface;
use FOSSCMS\Core\Kernel;
use FOSSCMS\Core\Services\PageService;
use FOSSCMS\Core\Services\RoutingService;

class AdminExtension implements ExtensionInterface
{

    public function initialize()
    {
        // Get all our services
        /** @var Kernel $kernel */
        $kernel = Kernel::instance();

        /** @var PageService $page */
        $page = $kernel->getService("page");

        /** @var RoutingService $routing */
        $route = $kernel->getService("route");

        // Add our template directory
        $page->addTemplateDirectory("root://code/Admin/content/pages");

        // Register all our routes
        $route->addRoute("/admin", "FOSSCMS\\Admin\\Controllers\\LoginController", "login");

        // Register our service
        $kernel->registerService("admin", AdminService::class);

        // Retrieve our newly created service
        /** @var AdminService $service */
        $service = $kernel->getService("admin");

        // Add sidebar entries
        $service->addSidebarEntry("/admin/meme", "Meme");
        $service->addSidebarEntry("/admin/test", "Test");
        $service->addSidebarEntry("/admin/filler", "Filler");

        // Register our event handlers
        $kernel->on("rendering.start", function() use ($page, $service) {
            $page->addGlobalVariable("admin", [
                "sidebar" => $service->getSidebarEntries()
            ]);
        });
    }
}