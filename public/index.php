<?php

require_once __DIR__ . "/../vendor/autoload.php";

use FOSSCMS\Admin\AdminExtension;
use FOSSCMS\Core\Helpers\DebugUtils;
use FOSSCMS\Core\Kernel;

/** @var Kernel $kernel */
$kernel = Kernel::instance();
$kernel->addExtension(new AdminExtension());
$kernel->initializeAll();

/** @var \FOSSCMS\Core\Services\FilesystemService $fs */
$fs = $kernel->getService("fs");

/** @var \FOSSCMS\Core\Services\ConfigService $config */
// $config = $kernel->getService("config");
// DebugUtils::dump($config->get("system", "paths.cache", "yeet"));
// DebugUtils::dump($config->set("system", "paths.cache", "aaaaaaaaaaaaaaaa"));
// DebugUtils::dump($config->get("system", "paths.cache", "yeet"));
// die();

$db = $kernel->getService('database');
$db->createTable('test_table');
$db->insert('test_table');
DebugUtils::dump($db);
die();

///** @var \FOSSCMS\Core\Services\CacheService $cache */
//$cache = $kernel->getService("cache");
//
//$filename = $cache->forceCacheFile("content://pages/templates/index.twig");
//echo $filename;
//
///** @var \FOSSCMS\Core\Services\DatabaseService $db */
//$db = $kernel->getService("database");
//$db->createTable();

return $kernel->handleRequest();
